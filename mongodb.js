//CRUD App - CREATE REMOVE UPDATE DELETE

const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient
const ObjectID = mongodb.ObjectID

const connectionURL = 'mongodb://127.0.0.1:27017'
const databaseName = 'task-manager'

MongoClient.connect(connectionURL, {useNewUrlParser: true}, (error, client) => {
    if(error){
        console.log('Unable to connect to database')
    }
   const db = client.db(databaseName)
//    db.collection('users').updateOne({
//        _id: new ObjectID("5ea3dc34c7bb91b360db160b")
//    }, {
//        $set: {
//            name: 'Iftekhar'
//        }
//    }).then((result) => {
//        console.log(result)
//    }).then((error) => {
//        console.log(error)
//    })
//    db.collection('tasks').updateMany({}, {
//        $set: {
//             completed: false
//        }
//    }).then((res) => {
//     console.log(res)
//    }).catch((error) => {
//     console.log(error)
//    })

//    db.collection('users').deleteMany({ age: 25}).then((res) => {
//         console.log(res)
//    }).catch((error) => {
//         console.log(error)
//    })

   db.collection('tasks').deleteOne({
       description: 'Plant a tree'
   }).then((res)=> {
       console.log(res)
   }).catch((error) => {
       console.log(error)
   })

    // // db.collection('users').find({age : 25}).count((error, users) => {
    // //     console.log(users)
    // // })

    // db.collection('tasks').findOne({ _id: new ObjectID("5ea3dd659fcec6b37090d8e2")},(error, task)=>{
    //         console.log(task)
    // })
    
    // db.collection('tasks').find({completed: true}).toArray((error, tasks)=> {
    //         console.log(tasks)
    // })

//    db.collection('users').insertOne({
//        name: 'Barack',
//        age: 25
//    })
//     db.collection('users').insertMany([
//         {
//             name: 'Zain',
//             age: 26
//          },
//          {
//              name: 'Sohel',
//              age: 34
//          }
// ], (error, result) =>{
//     if(error){
//         console.log(error)
//     }
//     console.log(result.ops)
// })

    // db.collection('tasks').insertMany([{
    //     description: 'Plant a tree',
    //     completed: true

    // },{
    //     description: 'Water the gardens',
    //     completed: false
    // },{
    //     description: 'Put some tiles',
    //     completed: true
    // }
    //     ], (error, results) =>{
    //         if(error){
    //             console.log('There is an error')
    //         }
    //         console.log(results.ops)
    //     })
})