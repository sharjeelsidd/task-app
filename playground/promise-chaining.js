require('../src/db/mongoose')
const Task = require('../src/models/task')

const deleteTaskandCount = async (id) => {
    const task = await Task.findByIdAndDelete(id)
    const count = await Task.countDocuments({completed: false})
    return count
}

deleteTaskandCount('5ea51f389c2b43c34419e8a1').then((count)=>{
    console.log(count)
}).catch((e)=>{
    console.log(e)
})