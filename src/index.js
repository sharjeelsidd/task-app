const express = require('express')
require('./db/mongoose')
const User = require('./models/user')
const Task = require('./models/task')
const userRouter = require('./routers/user') 
const taskRouter = require('./routers/tasks')
const port = process.env.PORT || 3000
const app = express()

const multer = require('multer')

app.use(express.json())
app.use(userRouter)
app.use(taskRouter)


app.listen(port, ()=>{
    console.log('Server is up on port ' + port)
})



// const main = async() => {
//     // const task = await Task.findById('5ead367c82edf6391332319d')
//     // await task.populate('owner').execPopulate()
//     // console.log(task.owner)

//     const user = await User.findById('5eace217bf1b54342f1016f5')
//     await user.populate('tasks').execPopulate()
//     console.log(user.tasks)
// }

// main()