const express = require('express')
const router = new express.Router()
const User = require('../models/user')
const jwt = require('jsonwebtoken')
const auth = require('../middleware/auth')
const { sendWelcomeEmail } = require('../emails/account')
const multer = require('multer')

router.get('/test', (req, res) => {
    res.send('This works')
})


router.post('/users',async (req, res) => {
    const user = new User(req.body)
    try {
        await user.save()
        sendWelcomeEmail(user.email, user.name)
        const token = await user.generateAuthToken()
        res.status(201).send({ user, token })
    }
    catch (e) {
        res.status(400).send(e)
    }
})

router.post('/users/login', async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password)
        const token = await user.generateAuthToken()
        res.send({ user, token })
    } catch (e) {
        res.status(400).send(e)
    }
})

router.post('/users/logout', auth, async(req, res)=>{
    try{
        req.user.tokens = req.user.tokens.filter((token)=>{
            return token.token !== req.token
        })
        await req.user.save()
        res.send()

    }catch(e){  
        res.status(500).send()
    }
})

router.post('/users/logoutAll', auth, async(req, res)=>{
    try{
        req.user.tokens = []
        await req.user.save()
        res.send()
    }catch(e){
        res.status(500).send()
    }
})




router.get('/users/me', auth, async(req, res)=>{
    res.send(req.user)
})
router.get('/users', auth, async (req, res) => {
    try {
        const users = await User.find({})
        res.send(users)
    }
    catch (e) {
        res.send(500).send(e)
    }
})

// router.get('/users/:id', async (req, res) => {
//     const _id = req.params.id
//     try {
//         const user = await User.findById(_id)
//         if (!user) {
//             return res.status(404).send()
//         }
//         return res.status(200).send(user)
//     } catch (e) {
//         res.status(500).send()
//     }
// })

router.patch('/users/me', auth, async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['name', 'email', 'password', 'age']
    const isValidOperation = updates.every((update) => {
        return allowedUpdates.includes(update)
    })
    if (!isValidOperation) {
        return res.status(400).send({ error: 'Invalid Updates' })
    }
    try {
        // const user = await User.findById(req.user._id)
        updates.forEach((update) => {
            req.user[update] = req.body[update]
        })
        await req.user.save()
     
        res.status(200).send(req.user)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.delete('/users/me', auth, async (req, res) => {
    try {
        // const user = await User.findByIdAndDelete(req.user._id)
        // if (!user) {
        //     return res.status(404).send()
        // }

        await req.user.remove()
        res.send(req.user)
    } catch (e) {
        res.status(500).send(e)
    }
})

const upload = multer({
    dest: 'avatar',
    limits: {
        fileSize: 1000000
    }, 
    fileFilter(req, file, cb){
        if(!file.originalname.match(/\.(jpg|png)$/)){
            return cb(new Error('Please upload a Image file'))
        }
        cb(undefined, true)
    }
})
router.post('/users/me/avatar', auth, upload.single('avatars'),  async (req, res) => {
   req.user.avatar = req.file.buffer
   await req.user.save()
   res.send()
}, (error,req, res,next)=>{
    res.status(400).send({ error: 'Please upload an Image'})
})

router.get('/users/me/avatar', async (req, res) => {
    try{
        const user = await User.findById(req.params.id)
        if(!user | !user.avatar){
            throw new Error()
        }
        res.set('Content-Type', 'images/jpg')
        res.status(200).send(user.avatar)
    }catch(e){
        res.status(404).send()
    }
})


router.delete('/users/me/avatar', auth, async(req, res) => {
    req.user.avatar = undefined;
    await req.user.save()
    res.send()
})
module.exports = router