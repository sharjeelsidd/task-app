const mongoose = require('mongoose')
const validator = require('validator')

mongoose.connect('mongodb://127.0.0.1:27017/task-manager-api', {
    useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true 
})

// const User = mongoose.model('User', {
//     name: {
//         type: String,
//         required: true,
//         trim: true
//     },
//     email: {
//         type: String, 
//         required: true,
//         trim: true,
//         lowercase: true, 
//         validate(value){
//             if(!validator.isEmail(value)){
//                 throw new Error('Email is invalid')
//             }
//         }
//     },
//     age: {
//         type: Number, 
//         default: 0,
//         validate(value){
//             if(value < 0){
//                 throw new Error('Age should be a positve number')
//             }
//         }
//     },
//     password: {
//         type: String,
//         require: true,
//         trim: true,
//         minlength: 7,
//         validate(value){
//             if(value.toLowerCase().includes('password')){
//                 throw new Error('Password cannot contains password')
//             }
//         }
//     }

// })



// const me = new User({
//     name: 'Sharjeel',
//     email: 'KFJGSKFJ256@gmail.com',
//     password: 'VBForLife@12',
//     age: 45
// })

// me.save().then(()=>{
//     console.log(me)
// }).catch((error)=>{
//     console.log(error)
// })




// const Task = mongoose.model('Task', {
//     description: {
//         type: String,
//         required: true,
//         trim: true
//     },
//     completed: {
//         type: Boolean,
//         default: false
//     }
// })

// const task = new Task({
//     description: 'Chill',
//     completed: true
// })


// task.save().then(()=> {
//     console.log(task)
// }).catch((error) => {
//     console.log(error)
// })

